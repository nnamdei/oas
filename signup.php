<?php
error_reporting(0);

?>



<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Student Registration</title>
<link rel="stylesheet" href="bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap/bootstrap-theme.min.css">
<script src="bootstrap/jquery.min.js"></script>
<script src="bootstrap/bootstrap.min.js"></script>
<script language="javascript" type="text/javascript"
	src="jquery/jquery-1.10.2.js"></script>
<script language="javascript" type="text/javascript"
	src="jquery/jquery-ui.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
<!--<link type="text/css" rel="stylesheet" href="css/sign.css"></link>-->




<script type="text/javascript">
        function validate()
        {
            $('#signup input[type="text"]').each(function() {
                if(this.required)
                {
                    if(this.value=="")
                        $(this).addClass("inpterr");
                    else
                        $(this).addClass("inpterrc");
                }

                if($(this).attr("VT")=="NM")
                {
                    if((!isAlpha(this.value)) && this.value!="")
                    {
                       alert("Only Aplhabets Are Allowed . . .");
                       $(this).focus();
                    }
                }

                        if($(this).attr("VT")=="PH")
                        {
                                if((!isPhone(this.value)) && this.value!="")
                                {
                                        alert("Check the format . . .");
                                        $(this).focus();
                                }
                        }

                        if($(this).attr("VT")=="EML")
                        {
                                if(!IsEmail($(this).val()) && this.value!="")
                                {
                                        alert("Invalid Email . . . .");
                                        $(this).focus();
                                }
                        }

                        if($(this).attr("VT")=="PIN")
                        {
                                if((!IsPin(this.value)) && this.value!="")
                                {
                                        alert("Invalid Pin Code . . . .");
                                        $(this).focus();
                                }
                        }

            });
        }

                function isAlpha(x)
                {
                    var re = new RegExp(/^[a-zA-Z\s]+$/);
                    return re.test(x);
                }

		function isPhone(x)
		{

			var ph = new RegExp (/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/);
			//if(!ph.length<10)
			return ph.test(x);
		}

		function IsEmail(x)
		{
  			var em = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);
  			return em.test(x);
		}

		function IsPin(x)
		{
  			var pin = new RegExp (/^\d{6}$/);

  			return pin.test(x);
		}
        </script>

<style type="text/css">
.inpterr {
	border: 1px solid red;
	background: #FFCECE;
}

.inpterrc {
	border: 1px solid black;
	background: white;
}
</style>


</head>


<body>
	<div class="container">


	<div style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2"><form class="form-horizontal" id="signup" action="signupconfirm.php" method="post">

         <div class="panel-title">Register </div>

		<div id="dvlogin" style="box-shadow: 0px 5px 10px #999999">
                    <?php

?>
            </div>


		<div id="dmid"></div>
		<div id="ddown">

			<div id="dleft">
				<input type="hidden" id="stid" name="stid" value="">
				<input type="hidden" id="stpw" name="stpw" value="">
			</div>

					<div class="form-group"> <input autocomplete="off" class="form-control" type="text" id="in_name" name="in_name" VT="NM" required="true" placeholder="Name"> </div>
					<div class="form-group"><input class="form-control" autocomplete="off" type="text" id="in_dob"
							name="in_dob" required="true" placeholder="Date of Birth"> <script type="text/javascript">
                            $(function() {
                            $("#in_dob").datepicker({ dateFormat: 'yy-mm-dd', yearRange:'-25:+0', changeYear:true, changeMonth:true});
                        });
                         </script></div>

                     <div class="form-group"><input autocomplete="off" class="form-control" type="email" id="in_eml" name="in_eml" VT="EML" required="true" placeholder="Email Address"></div>
				     <div class="form-group"><input autocomplete="off" class="form-control" type="number" id="in_mob" name="in_mob" VT="PH" required="true" maxlength="13" placeholder="Mobile Number"></div>


				     <div class="form-group">
<img style="margin-bottom: 15px;" src="captcha.php" style="height: 1.8em;" >
                        <input autocomplete="off" class="form-control" type='text'
							id='txt_captcha' name='txt_captcha' placeholder="Enter the code"
							required="true">
						</div>
						<div class="form-group"><input type="submit" id="in_sub" name="in_sub"
							onclick="validate();" value="SIGN UP"
							class="toggle btn btn-primary">
<div style="padding-top:15px; font-size:85%" >
                                            Already have an account?
                                        <a href="index.php">
                                            Login here
                                        </a>
                                        </div>
                        </div>




	</form></div>
</div>
</body>
</html>
