

<!DOCTYPE html>
<html>
<head>
    <title>Search results</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <link rel="stylesheet" href="bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap/bootstrap-theme.min.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="bootstrap/jquery.min.js"></script>
<script src="bootstrap/bootstrap.min.js"></script>
</head>
<body>
<div class="container" style="padding-top: 20px;"><img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt="" class="img-responsive center-block"></div>

	<?php
$connect = mysqli_connect("localhost", "root", "", "oas");
if ($connect) {

	if (isset($_POST['query'])) {

		// $search = "ndf okoooo";
		$search = mysqli_real_escape_string($connect, $_POST['query']);

		$name = isset($_GET['query']) ? trim($_GET['query']) : '';

		// $name
		$query = "SELECT distinct

					(M.`s_id`) as `s_id`,
                     M.`s_mark`,
                     U.`s_name`
                     FROM `t_usermark` as M
                    JOIN `t_user_data` as U on M.s_id = U.s_id  WHERE U.`s_id` = '$search'";

		$result = mysqli_query($connect, $query);

		echo "<div class='container'>";
		echo "<h2>Prismax Institute</h2>";

		echo "<p>Search Result for student</p>  ";
		echo "<div class='table-responsive'>";

		echo "<table class='table table-bordered'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th>Registration ID</th>";
		echo "<th>Student Name</th>";
		echo "<th>Final Score</th>";
		echo "</tr>";
		echo "</thead>";

		if ($result) {
			if (mysqli_num_rows($result) > 0) {
				/*echo "
					                    <table>
					                        <tr>
					                            <th>ID</th>
					                            <th>Name</th>
					                            <th>Mark</th>
					                        </tr>
				*/
				while ($row = mysqli_fetch_array($result)) {
					$id = $row['s_id'];
					$mark = $row['s_mark'];
					$name = $row['s_name'];

					echo "<tr>";
					echo "<td>" . $id . "</td>";
					echo "<td>" . $name . "</td>";
					echo "<td>" . $mark . "</td>";
					echo "</tr>";

					/*echo "
						                            <tr>
						                                <td>$id</td>
						                                <td>$name</td>
						                                <td>$mark</td>
						                            </tr>
					*/

					// echo "<p>$id</p><p>$mark</p><p>$name</p>";
				}

			} else {
				echo "No results";
			}

			echo "</table>";
		} else {
			echo mysqli_error($connect);
		}
	} else {
		echo "An error occured. Please try again";
	}
} else {
	echo "Database connection failed";
}

?>
<!--<?php
$query = $_GET['query'];
// gets value sent over search form

$min_length = 3;
// you can set minimum length of the query if you want

if (strlen($query) >= $min_length) {
	// if query length is more or equal minimum length then

	$query = htmlspecialchars($query);
	// changes characters used in html to their equivalents, for example: < to &gt;

	$query = mysql_real_escape_string($query);
	// makes sure nobody uses SQL injection

	$raw_results = mysql_query("SELECT `s_id`,`s_name`,`s_mark` from t_usermark where s_name like '%$query%'") or die(mysql_error());

	// * means that it selects all fields, you can also write: `id`, `title`, `text`
	// articles is the name of our table

	// '%$query%' is what we're looking for, % means anything, for example if $query is Hello
	// it will match "hello", "Hello man", "gogohello", if you want exact match use `title`='$query'
	// or if you want to match just full word so "gogohello" is out use '% $query %' ...OR ... '$query %' ... OR ... '% $query'

	if (mysql_num_rows($raw_results) > 0) {
		// if one or more rows are returned do following

		while ($results = mysql_fetch_array($raw_results)) {
			// $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop

			echo "<p><h3>" . $results['s_id'] . "</h3>" . $results['s_name'] . $results['s_mark'] . "</p>";
			// posts results gotten from database(title and text) you can also show id ($results['id'])
		}

	} else {
		// if there is no matching rows do following
		echo "No results";
	}

} else {
	// if query length is less than minimum
	echo "Minimum length is " . $min_length;
}
?>
-->
</body>
</html>

